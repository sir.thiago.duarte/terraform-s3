# finding the credentials
provider "aws" {
#    profile = "default"
    region = var.region
    access_key = var.access_key
    secret_key = var.secret_key
}

# creating s3

resource "aws_s3_bucket" "tf_bucket" {
    bucket = var.BUCKET_NAME
    acl = "private"
}